package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.api.service.IService;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.IndexIncorrectException;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public boolean existById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existById(id);
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(Comparator<? super M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public M findOneById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

    @Override
    public M remove(M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @Override
    public M removeById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Override
    public M removeByIndex(Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}
