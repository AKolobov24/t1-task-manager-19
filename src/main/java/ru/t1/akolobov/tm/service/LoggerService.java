package ru.t1.akolobov.tm.service;

import ru.t1.akolobov.tm.api.service.ILoggerService;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String LOG_DIRECTORY = "./log";

    private static final String COMMANDS_FILE = LOG_DIRECTORY + "/commands.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = LOG_DIRECTORY + "/errors.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = LOG_DIRECTORY + "/messages.xml";

    private static final LogManager MANAGER = LogManager.getLogManager();

    private static final Logger ROOT_LOGGER = Logger.getLogger("");

    private static final Logger COMMAND_LOGGER = getCommandLogger();

    private static final Logger ERROR_LOGGER = getErrorLogger();

    private static final Logger MESSAGE_LOGGER = getMessageLogger();

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(COMMAND_LOGGER, COMMANDS_FILE, false);
        registry(ERROR_LOGGER, ERRORS_FILE, true);
        registry(MESSAGE_LOGGER, MESSAGES_FILE, true);
    }

    public static Logger getCommandLogger() {
        return Logger.getLogger(COMMANDS);
    }

    public static Logger getErrorLogger() {
        return Logger.getLogger(ERRORS);
    }

    public static Logger getMessageLogger() {
        return Logger.getLogger(MESSAGES);
    }

    private void init() {
        try {
            MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
            final File logDirectory = new File(LOG_DIRECTORY);
            if (!logDirectory.exists()) logDirectory.mkdir();
        } catch (final IOException e) {
            ROOT_LOGGER.severe(e.getMessage());
        }
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            ROOT_LOGGER.severe(e.getMessage());
        }
    }

    @Override
    public void info(String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.info(message);
    }

    @Override
    public void debug(String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.fine(message);
    }

    @Override
    public void command(String message) {
        if (message == null || message.isEmpty()) return;
        COMMAND_LOGGER.info(message);
    }

    @Override
    public void error(Exception e) {
        if (e == null) return;
        ERROR_LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }

}
