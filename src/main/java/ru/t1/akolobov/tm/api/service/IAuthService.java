package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}
