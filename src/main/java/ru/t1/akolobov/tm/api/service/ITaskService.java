package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Sort sort);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, Status status);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
