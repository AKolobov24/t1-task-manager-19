package ru.t1.akolobov.tm.api.service;

import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Sort sort);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, Status status);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}
