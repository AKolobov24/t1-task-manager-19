package ru.t1.akolobov.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Access denied...");
    }

}
