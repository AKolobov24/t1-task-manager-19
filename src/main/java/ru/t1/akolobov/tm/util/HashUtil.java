package ru.t1.akolobov.tm.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    String SECRET = "1324";

    Integer ITERATIONS = 4123;

    static String salt(final String value) {
        if (value == null) return null;
        String result = value;
        for (int i = 0; i < ITERATIONS; i++) {
            result = sha256(SECRET + result + SECRET);
        }
        return result;

    }

    static String sha256(final String value) {
        if (value == null) return null;
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
            final StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                final String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
