package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskDisplayByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-display-by-id";

    public static final String DESCRIPTION = "Find task by Id and display.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        displayTask(task);
    }

}
