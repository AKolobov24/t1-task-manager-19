package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-index";

    public static final String DESCRIPTION = "Start task.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeStatusByIndex(index, Status.IN_PROGRESS);
    }

}
