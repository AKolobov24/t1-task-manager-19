package ru.t1.akolobov.tm.command;

import ru.t1.akolobov.tm.api.model.ICommand;
import ru.t1.akolobov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += " : " + description;
        return result;
    }

}