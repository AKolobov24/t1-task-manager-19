package ru.t1.akolobov.tm.command.user;

import ru.t1.akolobov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change current user password.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
