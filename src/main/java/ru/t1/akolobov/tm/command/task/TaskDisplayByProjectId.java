package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskDisplayByProjectId extends AbstractTaskCommand {

    public static final String NAME = "task-display-by-project-id";

    public static final String DESCRIPTION = "Find tasks by project Id and display task list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY TASK BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String id = TerminalUtil.nextLine();
        final List<Task> taskList = getTaskService().findAllByProjectId(id);
        renderTaskList(taskList);
    }

}
