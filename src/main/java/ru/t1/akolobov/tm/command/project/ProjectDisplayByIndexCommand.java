package ru.t1.akolobov.tm.command.project;

import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-display-by-index";

    public static final String DESCRIPTION = "Find project by Index and display.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[DISPLAY PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        displayProject(project);
    }

}
