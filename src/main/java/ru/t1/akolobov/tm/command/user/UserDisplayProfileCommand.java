package ru.t1.akolobov.tm.command.user;

import ru.t1.akolobov.tm.model.User;

public final class UserDisplayProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-display-profile";

    public static final String DESCRIPTION = "Display current user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[DISPLAY USER PROFILE]");
        displayUser(user);
    }

    @Override
    protected void displayUser(final User user) {
        super.displayUser(user);
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
    }

}
