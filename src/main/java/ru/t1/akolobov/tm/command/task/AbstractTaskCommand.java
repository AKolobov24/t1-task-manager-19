package ru.t1.akolobov.tm.command.task;

import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTaskList(final List<Task> taskList) {
        int index = 1;
        for (final Task task : taskList) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void displayTask(Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
